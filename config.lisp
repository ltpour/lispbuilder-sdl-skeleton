;; configuration
(setf *width* 800)        ; width of the screen. default 800.
(setf *height* 600)       ; height of the screen. default 600.
(setf *fullscreen-p* NIL)   ; fullscreen. possible values t for true and NIL for false. default NIL.
(setf *sound-on-p* t)     ; sound effects. possible values t for ON and NIL for OFF. default t.
(setf *sound-volume* 96)  ; sound effects volume. possible values [0, 128]. default 96.
(setf *music-on-p* t)     ; music. possible values t for ON and NIL for OFF. default t.
(setf *music-volume* 96)  ; music volume. possible values [0, 128]. default 96.
