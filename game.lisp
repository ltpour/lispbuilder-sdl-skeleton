;; Default config
(defvar *state* "start")
(defvar *scale* 10)
(defvar *widescreen-offset* 0)
(defvar *unscaled-width* 64)
(defvar *unscaled-height* 48)
(defvar *width* 800)
(defvar *height* 600)
(defvar *fullscreen-p* NIL)
(defvar *color* sdl:*white*)
(defvar *player-x* (/ *unscaled-width* 2))
(defvar *player-y* (/ *unscaled-height* 2))
(defvar *player-sprite* NIL)
(defvar *sound-on-p* t)
(defvar *sound-volume* 96)
(defvar *sound-effect-1* NIL)
(defvar *sound-effect-2* NIL)
(defvar *music-on-p* t)
(defvar *music-volume* 96)
(defvar *music* NIL)

(defun update-start ()

  ;; Begin game when left key is pressed
  (when (sdl:mouse-left-p)
    (setf *state* "game"))

  ;; Draw start screen
  (sdl:draw-box (sdl:rectangle-from-midpoint-* (/ *width* 2) (/ *height* 2) (- *width* (/ *width* 10)) (- *height* (/ *height* 2)))
		:color *color*)
  (sdl:draw-string-shaded-* "Click mouse to start" (/ *width* 2) (/ *height* 2) sdl:*red* sdl:*black*))

(defun update-game ()

  ;; Return to start screen when right key is pressed
  (when (sdl:mouse-right-p)
    (setf *state* "start"))

  ;; Change the color of the box when left button is down
  (when (sdl:mouse-left-p)
    (setf *color* (sdl:color :r (random 255) :g (random 255) :b (random 255))))
  
  ;; Move player
  (if (sdl:get-key-state :sdl-key-w)
      (decf *player-y* 0.2))
  (if (sdl:get-key-state :sdl-key-s)
      (incf *player-y* 0.2))
  (if (sdl:get-key-state :sdl-key-a)
      (decf *player-x* 0.2))
  (if (sdl:get-key-state :sdl-key-d)
      (incf *player-x* 0.2))

  ;; Keep player within screen
  (if (> *player-y* (- *unscaled-height* 4))
      (setf *player-y* (- *unscaled-height* 4)))
  (if (< *player-y* 0)
      (setf *player-y* 0))
  (if (> *player-x* (- *unscaled-width* 2))
      (setf *player-x* (- *unscaled-width* 2)))
  (if (< *player-x* 0)
      (setf *player-x* 0))

  ;; Draw green background
  (sdl:draw-box (sdl:rectangle-from-edges-* *widescreen-offset* 0 (+ *widescreen-offset* (* *scale* *unscaled-width*)) (* *scale* *unscaled-height*)) :color sdl:*green*)

  ;; Draw mouse controlled box
  (sdl:draw-box (sdl:rectangle-from-midpoint-* (sdl:mouse-x) (sdl:mouse-y) 20 20)
		:color *color*)

  ;; Scale and draw player surface on screen
  (sdl:draw-surface-at-* *player-sprite* (round (+ *widescreen-offset* (* (round *player-x*) *scale*))) (round (* (round *player-y*) *scale*)))

  ;; Debug string
  (sdl:draw-string-shaded-* (write-to-string (sdl-mixer:music-playing-p)) (/ *width* 2) (/ *height* 2) sdl:*red* sdl:*black*))

(defun play-game ()
  
    ;; Overwrite default config
    (when (probe-file "config.lisp")
      (load "config.lisp"))

    ;; Set sound off if files missing
    (when (not (and (probe-file "sounds/sf1.ogg") (probe-file "sounds/sf2.ogg")))
      (setf *sound-on-p* NIL))
    (when (not (probe-file "sounds/bgm.ogg"))
      (setf *music-on-p* NIL))

    ;; Correct scale and offset
    (if (< (/ *width* *height*) (/ 4 3))
	(setf *width* (round (* *height* (/ 4 3)))))
    (if (> (/ *width* *height*) (/ 4 3))
	(setf *widescreen-offset* (round (/ (- *width* (* *height* (/ 4 3))) 2))))
    (setf *scale* (/ *height* *unscaled-height* ))

    (sdl:with-init ()

      ;; Create window
      (sdl:window *width* *height* :title-caption "Window title goes here" :fullscreen *fullscreen-p*)
      (setf (sdl:frame-rate) 60)

      ;; Create sprite
      (setf *player-sprite* (sdl:create-surface (* 2 *scale*) (* 4 *scale*)))
      (sdl:draw-box (sdl:rectangle-from-edges-* 0 0 (* 2 *scale*) (* 4 *scale*))
		    :color sdl:*blue* :surface *player-sprite*)
      
      ;; Initialize fonts
      (unless (sdl:initialise-default-font sdl:*ttf-font-vera*)
	(error "Cannot initialize the default font."))

      ;; Load sounds and music and play music
      (when (or *music-on-p* *sound-on-p*)
	(sdl-mixer:OPEN-AUDIO)
	(when *music-on-p*
	  (setf (sdl-mixer:music-volume) *music-volume*)
	  (setf *music* (sdl-mixer:load-music "sounds/bgm.ogg"))
	  (sdl-mixer:play-music *music* :loop t))
	(when *sound-on-p*
	  (sdl-mixer:reserve-channels 1) ; Reserve channel 0 for looping sound effect
	  (setf *sound-effect-1* (sdl-mixer:load-sample "sounds/sf1.ogg"))
	  (setf *sound-effect-2* (sdl-mixer:load-sample "sounds/sf2.ogg"))
	  (setf (sdl-mixer:sample-volume *sound-effect-1*) *sound-volume*)
	  (setf (sdl-mixer:sample-volume *sound-effect-2*) *sound-volume*)))
	  
      (sdl:with-events ()

	(:quit-event ()
		     (when (or *music-on-p* *sound-on-p*)
		       (when *music-on-p*
			 (sdl-mixer:Halt-Music)
			 (sdl-mixer:free *music*))
		       (when *sound-on-p*
			 (sdl-mixer:free *sound-effect-1*))
		       (sdl-mixer:CLOSE-AUDIO t))
		     t)

	(:key-down-event (:key key)		
			 (when (sdl:key= key :sdl-key-escape)
			   (cond
			     ((equal *state* "start")
			      (sdl:push-quit-event))
			     ((equal *state* "game")
			      (setf *state* "start"))))
			 (when (sdl:key= key :sdl-key-return)
			   (cond
			     ((equal *state* "start")
			      (setf *state* "game"))))
			 (when (or (sdl:key= key :sdl-key-w) (sdl:key= key :sdl-key-s) (sdl:key= key :sdl-key-a) (sdl:key= key :sdl-key-d))
			   (if (and *sound-on-p* (not (sdl-mixer:sample-playing-p 0)))
			       (sdl-mixer:play-sample *sound-effect-1* :channel 0 :loop t))))

	(:key-up-event (:key key)
		       (when (or (sdl:key= key :sdl-key-w) (sdl:key= key :sdl-key-s) (sdl:key= key :sdl-key-a) (sdl:key= key :sdl-key-d))
			 (if (and *sound-on-p* (sdl-mixer:sample-playing-p 0) (not (or (sdl:get-key-state :sdl-key-w) (sdl:get-key-state :sdl-key-s) (sdl:get-key-state :sdl-key-a) (sdl:get-key-state :sdl-key-d))))
			     (sdl-mixer:halt-sample :channel 0 :fade 5))))		       
	 
	(:idle ()
	       
	       ;; Clear screen every frame
	       (sdl:clear-display sdl:*black*)

	       ;; Update and draw the state we're in
	       (cond
		 ((equal *state* "start") 
		  (update-start))
		 ((equal *state* "game")
		  (update-game)))

	       ;; Redraw screen every frame
	       (sdl:update-display)))))
