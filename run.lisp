;; loads all required files and runs the game
(ql:quickload "lispbuilder-sdl")
(ql:quickload "lispbuilder-sdl-image")
(ql:quickload "lispbuilder-sdl-ttf")
(ql:quickload "lispbuilder-sdl-mixer")
(load (compile-file "game"))
(load (compile-file "main"))
(play-game)
