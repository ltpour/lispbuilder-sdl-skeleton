# lispbuilder-sdl-skeleton

### Summary

A skeleton for a game. A work in stagnant progress.

This skeleton was written to be a starting point for my personal LISP game projects. It is not very actively maintained. It is also not currently under any license, but feel free to use it as you wish.

### Requirements

A Common Lisp compiler, Quicklisp and the following SDL 1.2 libraries:

- sdl
- sdl-image
- sdl-ttf
- sdl-mixer

### Running the game from the SBCL REPL

```
(load "build.lisp")
```

### Building an executable

```
(load "run.lisp")
```

The project is tested to work with SBCL 1.3.16, it may not work with earlier versions or other LISP implementations.

See the Lispbuilder wiki for more information.
